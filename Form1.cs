namespace quote_project_999
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            try
            {
                txtQuotes.Text = " oef 1, werken met allemaal in dezelfde file => niet doen";
                //oef 2

                var hans = new Hans();
                txtQuotes.Text = hans.ToString()+ Environment.NewLine;

                var a = new Abdul();
                txtQuotes.Text += a.ToString() + Environment.NewLine;

                var b = new Bart();
                txtQuotes.Text += b.ToString() + Environment.NewLine;
                var c = new Bernd();
                txtQuotes.Text += c.ToString() + Environment.NewLine;

                var d = new Cedric();
                txtQuotes.Text += d.ToString() + Environment.NewLine;
                var z = new Gilles();
                txtQuotes.Text += z.ToString() + Environment.NewLine;

                var f = new Illya();
                txtQuotes.Text += f.ToString() + Environment.NewLine;
                var g = new Jeffrey();
                txtQuotes.Text += g.ToString() + Environment.NewLine;

                var h = new Robin();
                txtQuotes.Text += h.ToString() + Environment.NewLine;
                var i = new Tycho();
                txtQuotes.Text += i.ToString() + Environment.NewLine;

                var j = new Yorben();
                txtQuotes.Text += j.ToString() + Environment.NewLine;

            }
            catch
            {
                txtQuotes.Text = "Helaas pindakaas!";
            }

            
        }
    }
}